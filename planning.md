* [x] Feature 3
  * [x] create model TodoList
    * [x] attributes are name and created_on
* [x] Feature 4
  * [x] in admin.py register TodoList model 
* [x] Feature 5
  * [x] create TodoItem model
    * [x] task with CharField(max_length=100)
    * [x] due_date null=True type date time
    * [x] s_complete default=False time boolean
    * [x] list foreignkey related_name items CASCade
    * [x] create a __str__ functino with task as the return
* [x] Feature 6: add TodoItem to admin
